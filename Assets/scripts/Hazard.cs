﻿using UnityEngine;
using System.Collections;

public class Hazard : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			var controller = thing.gameObject.GetComponent<PlayerController>();
			controller.dead = true;
		}
	}

	void OnTriggerEnter2D(Collider2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			var controller = thing.gameObject.GetComponent<PlayerController>();
			controller.dead = true;
		}
	}
}
