﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	private float timeout = 1f;
	
	void FixedUpdate()
	{
		timeout -= 1*Time.deltaTime;
		if(timeout <= 0)
		{
			Object.Destroy(this.gameObject);
		}
	}
	
	void OnCollisionEnter2D(Collision2D thing)
		{
			if (thing.gameObject.tag == "Goomba")
			{
				Destroy(thing.gameObject);
				Object.Destroy(this.gameObject);
			}
			else
			{
				Object.Destroy(this.gameObject);
			}
		}
}
