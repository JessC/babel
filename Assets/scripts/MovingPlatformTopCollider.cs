﻿using UnityEngine;
using System.Collections;

public class MovingPlatformTopCollider : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D thing)
	{
		if (thing.gameObject.tag == "Player") 
		{
			Debug.Log ("ENTERING");
			thing.transform.parent = gameObject.transform;
		}
	}
	
	void OnTriggerExit2D(Collider2D thing)
	{
		if (thing.gameObject.tag == "Player") 
		{
			Debug.Log ("EXITING");
			thing.transform.parent = null;
		}
	}
}
