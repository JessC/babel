﻿using UnityEngine;
using System.Collections;

public class FootCollider : MonoBehaviour {

		public bool collision = false;
        public bool jumpAid = false;
		
        void OnCollisionEnter2D(Collision2D thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = true;
			}

		}
		
		void OnCollisionExit2D(Collision2D thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = false;
			}
		}
        
        
        void OnTriggerEnter2D(Collider2D thing)
        {
            if (thing.gameObject.tag == "JumpAid")
            {
                jumpAid = true;
            }
        }
	
        void OnTriggerExit2D(Collider2D thing)
        {
            if (thing.gameObject.tag == "JumpAid")
            {
                jumpAid = false;
            }
	}
}