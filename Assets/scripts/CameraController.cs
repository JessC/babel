﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform playerPos;
	public float cameraTrackingSpeed = 5f;
	// Update is called once per frame
	void Update () {
	
		float yInput = Input.GetAxisRaw("Vertical");
		
		Vector3 target = playerPos.position;
		target.y += 1.5f;
		
		if(yInput < 0)
			target.y -= 3;
		
		target.x = Mathf.Lerp(transform.position.x, target.x, cameraTrackingSpeed*Time.deltaTime);
		target.y = Mathf.Lerp(transform.position.y, target.y, (cameraTrackingSpeed*0.75f)*Time.deltaTime);
		target.z = -17.55147f;
		transform.position = target;
	}
}
