﻿using UnityEngine;
using System.Collections;

	
public class movingPlatform : MonoBehaviour 
{
	//directions and states
	public enum direction{left, right, up, down};
	public enum platState{sleep, movingLeft, movingRight, movingUp, movingDown};

	//designer variables
    public direction startingDirection =  direction.right;
	public platState currentState = platState.sleep;
	public float patrolDistance;
	public float speed;
	public float sleep;
	 //my variables
	float sleepyTime;
	direction lastDirection;
	float distancePatrolled;
    
	void Start()
	{
		sleepyTime = sleep;
		distancePatrolled = patrolDistance;
        
        if(startingDirection == direction.right)
            lastDirection = direction.left;
        else if(startingDirection == direction.left)
            lastDirection = direction.right;
        else if(startingDirection == direction.down)
            lastDirection = direction.up;
        else
            lastDirection = direction.down;
            
	}

	// Update is called once per frame
	void FixedUpdate()
	{
	
		switch(currentState)
		{
		
			case platState.sleep:
			    sleepyTime -= Time.deltaTime;
				if(sleepyTime <= 0)
				{
					sleepyTime = sleep;
					distancePatrolled = patrolDistance;
					
					if(lastDirection == direction.left)
						currentState = platState.movingRight;
					else if(lastDirection == direction.right)
						currentState = platState.movingLeft;
                    else if(lastDirection == direction.up)
						currentState = platState.movingDown;
                    else
                        currentState = platState.movingUp;
				}
				break;
                
             case platState.movingUp:
			
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.y += speed*Time.deltaTime;
				    transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					lastDirection = direction.up;
					currentState = platState.sleep;
				}
				break;
                
             case platState.movingDown:
			
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.y -= speed*Time.deltaTime;
				    transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					lastDirection = direction.down;
					currentState = platState.sleep;
				}
				break;
				
			case platState.movingLeft:
			
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.x -= speed*Time.deltaTime;
			    	transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					lastDirection = direction.left;
					currentState = platState.sleep;
				}
				break;
				
			case platState.movingRight:
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.x += speed*Time.deltaTime;
			    	transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					lastDirection = direction.right;
					currentState = platState.sleep;
				}
				break;
        }
    }
}