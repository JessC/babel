﻿using UnityEngine;
using System.Collections;

	
public class BasicEnemy : MonoBehaviour 
{
	//directions and states
	public enum direction{left, right};
	public enum enemyState{sleep, movingLeft, movingRight, attacking};

	//designer variables
    public direction startingDirection =  direction.right;
	public enemyState currentState = enemyState.sleep;
	public float patrolDistance;
	public float speed;
	public float sleep;
	public Transform attack;
	 //my variables
	float sleepyTime;
	direction lastDirection;
	float distancePatrolled;
	bool Triggered;
	float attackDuration = 1f;
	float attackTimer;
	
	void Start()
	{
		sleepyTime = sleep;
		distancePatrolled = patrolDistance;
		Triggered = false;
		lastDirection = (startingDirection == direction.right) ? direction.left : direction.right;

		attack = transform.FindChild ("Spoon");
	}
	// Update is called once per frame
	void FixedUpdate()
	{
	
		switch(currentState)
		{
		
			case enemyState.sleep:
			    sleepyTime -= Time.deltaTime;
				if(sleepyTime <= 0)
				{
					sleepyTime = sleep;
					distancePatrolled = patrolDistance;
					
					if(lastDirection == direction.left)
						currentState = enemyState.movingRight;
					else
						currentState = enemyState.movingLeft;
				}
				break;
				
			case enemyState.movingLeft:
			
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.x -= speed*Time.deltaTime;
					transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					flip();
					lastDirection = direction.left;
					currentState = enemyState.sleep;
				}
				break;
				
			case enemyState.movingRight:
				if(distancePatrolled >= 0)
				{
					Vector2 move = transform.position;
					move.x += speed*Time.deltaTime;
					transform.position = move;
					distancePatrolled -= speed*Time.deltaTime;
				}
				else
				{
					flip();
					lastDirection = direction.right;
					currentState = enemyState.sleep;
				}
				break;
				
			case enemyState.attacking:
				
				switch(lastDirection)
				{
					
					case direction.right:

						Vector2 attackMovement = attack.position;
						attackMovement.x -= 2.5f*Time.deltaTime;
						
						attack.position = attackMovement;
						
						attackTimer -= Time.deltaTime;
						if(attackTimer <= 0)
						{
							attack.position = transform.position;
							currentState = enemyState.movingLeft;
						}
						break;
						
					case direction.left:
 
						attackMovement = attack.position;
						attackMovement.x += 2.5f*Time.deltaTime;
						
						attack.position = attackMovement;
						
						attackTimer -= Time.deltaTime;
						if(attackTimer <= 0)
						{ 
							attack.position = transform.position;
							currentState = enemyState.movingRight;
						}
						break;
				
				}
			break;
				
		}
		
		//attack the player
		if(Triggered && currentState != enemyState.sleep && currentState != enemyState.attacking && distancePatrolled >=0.2f)
		{
			attackTimer = attackDuration;
			currentState = enemyState.attacking;
		}
		
	}
	
	void flip()
	{
		Vector3 flip = new Vector3(0,0);
		flip = transform.localScale;
		flip.x = -flip.x;
		transform.localScale = flip;
	}
	
	void OnTriggerEnter2D(Collider2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			Triggered = true;
			Debug.Log("hello");
		}
	}
	
	void OnTriggerExit2D(Collider2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			Triggered = false;
			Debug.Log("goodbye");
		}
	}
	
	void OnCollisionEnter2D(Collision2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			var controller = thing.gameObject.GetComponent<PlayerController>();
			controller.dead = true;
		}
	}
}
