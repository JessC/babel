﻿using UnityEngine;
using System.Collections;

public class SideCollider : MonoBehaviour {

		public bool collision = false;
		void OnCollisionEnter2D(Collision2D thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = true;
			}

		}
		
		void OnCollisionExit2D(Collision2D thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = false;
			}

		}
}