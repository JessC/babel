﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	enum state{onGround, Jumping, Falling, shooting, dieing};
	enum direction{left, right};

	private AudioSource audioSource;
	public AudioClip[] footstepClips;
	public AudioClip[] PlayerDeathClip;

	public Rigidbody2D projectile;
	public float bullet_speed = 12f;
	public float moveSpeed = 10f;
	public float jumpSpeed = 3f;
	public float jumpDampening = 1.6f;
	public float shootDelay = 0.5f;
	public SideCollider sideCol;
	public FootCollider footCol;
	[HideInInspector] public bool dead = false;
	float shootTimer;
	float deathTimer;
	bool shot;
	bool shootEnabled;
    bool jump;
	state currentState;
	direction currentDirection;
	Rigidbody2D rigidBody;
	Animator animator;
	
	// Use this for initialization
	void Start ()
	{
		rigidBody = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource> ();
		animator = GetComponent<Animator>();
		currentState = state.onGround;
		currentDirection = direction.right;
		shootTimer = shootDelay;
		deathTimer = 0.5f;
		shootEnabled = true;
        jump = false;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		float xInput = Input.GetAxis("Horizontal");
		float yInput = Input.GetAxisRaw("Vertical");
		bool shoot = Input.GetButton("Fire1");
		bool restart = Input.GetButton("Submit");
        if(yInput == 0)
            jump = false;
        
        
        //cheats
		if(Input.GetKey("t"))
		{
			moveSpeed += 2*Time.deltaTime;
			jumpSpeed += 2*Time.deltaTime;
		}
		
		if(restart)
			Application.LoadLevel(Application.loadedLevel);

		
		//dont walk into walls
		if(sideCol.collision)
		{
					switch (currentDirection)
					{
						case direction.right:
							if(xInput > 0)
							{
									xInput = 0;
							}
							break;
						case direction.left:
							if(xInput < 0)
							{	
									xInput = 0;
							}
							break;
					}
		}
		
		
		if(currentState != state.shooting && currentState != state.dieing)
		{
			//switching directions : )
			switch (currentDirection)
			{
				case direction.right:
					if(xInput < 0)
					{
						flip();
						currentDirection = direction.left;
					}
					break;
				case direction.left:
					if(xInput > 0)
					{	
						flip();
						currentDirection = direction.right;
					}
					break;
			}
		}
		
		
		switch (currentState)
		{
			
			//ground state
			case state.onGround:
			
				if(xInput != 0)
				{
					Vector2 walkForce = new Vector2(xInput * moveSpeed, rigidBody.velocity.y);
					rigidBody.velocity = walkForce;	
				}
				
				if(shoot  && shootEnabled == true)
				{
					shootTimer = shootDelay;
					shot = false;
					currentState = state.shooting;
				}
				if(shot && shootTimer >= 0)
					shootTimer -= 1*Time.deltaTime;
					
				if(yInput > 0 && jump == false)
				{
					Vector2 jumpForce = new Vector2(xInput * (moveSpeed/2), jumpSpeed);
					rigidBody.velocity = jumpForce;
					currentState = state.Jumping;
				}
			
				if(rigidBody.velocity.y < 0)
				{
					currentState = state.Falling;
				}
			
				break;
			
				//jump state
			case state.Jumping:
				if(jump == false)
                    jump = true;
                
				Vector2 jumpXForce = rigidBody.velocity;
				jumpXForce.x = xInput * (moveSpeed/2);
				rigidBody.velocity = jumpXForce;
				
				Vector2 jumpYForce = rigidBody.velocity;
				jumpYForce.y -= jumpDampening - (yInput/3);
				rigidBody.velocity = jumpYForce;
				
				if(shoot && shootEnabled == true)
				{
					shootTimer = shootDelay;

					Rigidbody2D bullet;
					Vector2 bulletSpawn = transform.position;
					Vector2 bulletSpeed = new Vector2(0,0);
				
					switch (currentDirection)
					{
						case direction.right:
						bulletSpawn.x += 0.8f;
						bulletSpeed.x = bullet_speed;
						break;
					case direction.left:
						bulletSpawn.x -= 0.8f;
						bulletSpeed.x = -bullet_speed;
						break;
					}
				
					bullet = Instantiate(projectile, bulletSpawn, transform.rotation) as Rigidbody2D;
					bullet.velocity = bulletSpeed;
					shot = true;
					shootEnabled = false;
				}
				if(shot && shootTimer >= 0)
					shootTimer -= 1*Time.deltaTime;
			
				if(rigidBody.velocity.y < 0)
				{
					currentState = state.Falling;
				}
			
				break;
			
				//fall state
			case state.Falling:
                if(footCol.jumpAid)
                {
                    Vector2 jumpForce = new Vector2(xInput * (moveSpeed/2), (jumpSpeed*1.3f));
					rigidBody.velocity = jumpForce;
					currentState = state.Jumping;
                }
            
				if(xInput != 0)
				{
					Vector2 fallForce = new Vector2(xInput * (moveSpeed/1.5f), rigidBody.velocity.y);
					rigidBody.velocity = fallForce;
				}
				
				if(shoot && shootEnabled == true)
				{
					shootTimer = shootDelay;

					Rigidbody2D bullet;
					Vector2 bulletSpawn = transform.position;
					Vector2 bulletSpeed = new Vector2(0,0);
				
					switch (currentDirection)
					{
						case direction.right:
						bulletSpawn.x += 0.8f;
						bulletSpeed.x = bullet_speed;
						break;
					case direction.left:
						bulletSpawn.x -= 0.8f;
						bulletSpeed.x = -bullet_speed;
						break;
					}
				
					bullet = Instantiate(projectile, bulletSpawn, transform.rotation) as Rigidbody2D;
					bullet.velocity = bulletSpeed;
					shot = true;
					shootEnabled = false;
				}
				if(shot && shootTimer >= 0)
					shootTimer -= 1*Time.deltaTime;
			
				if(footCol.collision)
				{
					currentState = state.onGround;
				}
			
				break;
				
			case state.shooting:
			
				if(rigidBody.velocity.y < 0)
				{
					currentState = state.Falling;
				}
				
				Vector2 shootingMovement = rigidBody.velocity;
				shootingMovement.x = shootingMovement.x/1.2f;
				shootingMovement.y = shootingMovement.y/1.2f;
				rigidBody.velocity = shootingMovement;
				
				shootTimer -= 1*Time.deltaTime;
				
				if(shot == false)
				{
					Rigidbody2D bullet;
					Vector2 bulletSpawn = transform.position;
					Vector2 bulletSpeed = new Vector2(0,0);
				
					switch (currentDirection)
					{
						case direction.right:
						bulletSpawn.x += 0.8f;
						bulletSpeed.x = bullet_speed;
						break;
					case direction.left:
						bulletSpawn.x -= 0.8f;
						bulletSpeed.x = -bullet_speed;
						break;
					}
				
					bullet = Instantiate(projectile, bulletSpawn, transform.rotation) as Rigidbody2D;
					bullet.velocity = bulletSpeed;
					shot = true;
					shootEnabled = false;
				}
				
				if(shootTimer <= 0)
				{
					currentState = state.onGround;
				}
				
				break;
				
			case state.dieing:
				Vector2 stop = new Vector2(0,0);
				rigidBody.velocity = stop;
				animator.SetBool ("Dead", true);
				
				if(deathTimer <= 0)
				{
					Destroy(this.gameObject);
					Application.LoadLevel(Application.loadedLevel);
				}
				deathTimer -= Time.deltaTime;
				break;
			
		}
		
		//allow the player to shoot again
		if(shoot == false && shootTimer <= 0)
		{
			shootEnabled = true;
		}
		
		if(dead)
			currentState = state.dieing;
		
		// Pass our parameters to our animator
		animator.SetFloat ("xVelocity", rigidbody2D.velocity.x);
		animator.SetFloat ("yVelocity", rigidbody2D.velocity.y);
		animator.SetBool ("Shooting", currentState == state.shooting);
		//Debug.Log(currentState);
		
		
	}

	public void PlayFootstepSound()
	{
		var clip = footstepClips[Random.Range (0, footstepClips.Length)];
		audioSource.PlayOneShot(clip);
	}
	public void PlayerDeathSound()
	{
		var clip = PlayerDeathClip[Random.Range (0, PlayerDeathClip.Length)];
		audioSource.PlayOneShot(clip);
	}

	void flip()
	{
		Vector3 flip = new Vector3(0,0);
		flip = transform.localScale;
		flip.x = -flip.x;
		transform.localScale = flip;
	}
}