﻿using UnityEngine;
using System.Collections;

public class PlayerDetector : MonoBehaviour {

	public bool collision = false;
	void OnCollisionEnter2D(Collision2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			collision = true;
		}
	}
	
	void OnCollisionExit2D(Collision2D thing)
	{
		if (thing.gameObject.tag == "Player")
		{
			collision = false;
		}

	}
}
